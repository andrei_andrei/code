<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactUsRequest;
use App\Notifications\HelloThereMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class ContactUsController extends Controller
{
    /**
     * Show contact us form
     * @return mixed
     */
    public function index()
    {
        return view('contact.index');
    }

    /**
     * Store contact us request
     * @param ContactUsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ContactUsRequest $request)
    {
        $request->store();

        Notification::route('mail', options_find('contact::email'))
            ->notify(new HelloThereMessage($request->all()));

        return back()->with('status', 'success')->withInput(['sent' => 'success']);
    }
}