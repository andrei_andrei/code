<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class HelloThereMessage extends Notification implements ShouldQueue
{
    use Queueable;

    public $notification;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->notification = (object)$data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('New message from Hello there form.')
            ->line('Name: ' . $this->notification->name)
            ->line('Email: ' . $this->emailize(
                    $this->notification->email
            ))
            ->line('Phone: ' . $this->notification->phone)
            ->line('Position: ' . $this->notification->position)
            ->line('Skills: ' . $this->notification->skills)
            ->line('Question: ' . $this->notification->question)
            ->salutation('-------------------------');
    }

    /**
     * @param $string
     * @return mixed
     */
    private function emailize($string)
    {
        $regex = '/\b(\S+@\S+\.\S+)\b/';
        $replace = '<a href="mailto:$1">$1</a>';

        return preg_replace($regex, $replace, $string);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
//
        ];
    }

}