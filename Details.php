<?php

namespace App\Services\Places;

use Terranet\Localizer\Locale;

class Details
{
    protected $json;

    protected $locale;

    public function __construct($json, Locale $locale = null)
    {
        $this->json = $json;
        $this->locale = $locale ?: \localizer\locale();
    }


    /**
     * @return array
     */
    public function export(): array
    {
        return [
            'place_id' => $this->id(),
            'name' => $this->name(),
            'geometry' => $this->geometry(),
            'country' => $this->country(),
            'city' => $this->city(),
            'status' => $this->success()
        ];
    }

    /**
     * Get Place ID.
     *
     * @return string
     */
    public function id(): string
    {
        return $this->json['result']['place_id'];
    }

    /**
     * Get place name.
     *
     * @return string
     */
    public function name(): string
    {
        return $this->json['result']['name'];
    }

    /**
     * Get place geometry.
     *
     * @return array
     */
    public function geometry(): array
    {
        return (array)$this->json['result']['geometry']['location'];
    }


    /**
     * Get country name
     * @return string
     */
    public function country(): string
    {
        $country = array_filter($this->json['result']['address_components'], function ($item) {
            return 'country' === $item['types'][0];
        });

        $country = reset($country);

        return $country['long_name'];
    }

    /**
     * Get city name
     * @return string
     */
    public function city(): string
    {
        $country = array_filter($this->json['result']['address_components'], function ($item) {
            return 'locality' === $item['types'][0];
        });

        $country = reset($country);

        return isset($country['long_name']) ? $country['long_name'] : '';
    }

    public function success()
    {
        return "OK" === $this->json['status'];
    }
}